1. Scénario _à la carte_  ?

Il faut le figer -- suggérer un ordre.

- Rédiger une introduction (Maxime se propose de commencer)
- Faire _la fiche modèle exercice résolu_ (plusieurs ?) (seb)

Structure _module_ possible :

1. Fiche modèle (unique ?) + quiz
2. Production + _correction par les pairs_ (réfléchir au côté technique)

Une fiche type possible (à la fois pour faire l'exercice et pour la production)

exemples : https://drive.google.com/drive/u/0/folders/1PkiCWzOGXEGlmGYUAplzk4TsBmYF3PKZ

- Thématique
- Notions liées
- Résumé
- Pré-requis
- Objectifs
- Contexte 
- Matériel nécessaire

Demande MHC : préciser le plan (pour nous, pour le montage notamment) (seb regardera ça)

TV : les interviews
- David (fait)
- Collègue (capéssienne) par qui ? (Maxime et/ou David et/ou Thierry)
- Maxime

Violaine Prince : cours sur épistémologie, préparation mémoire CAPES (dossier épreuve oral) etc. (6h) -- TV doit approfondir

SH : Penser à l'endroit où insérer le livre





