# Données en tables

## Activité 2 : Manipuler deux tables de tailles _réelles_

### Contexte et pré-requis

Vous êtes en séance de TP une semaine après avoir effectué la séance découverte des formats CSV et TSV (et assimilés). Vous avez 2h pour faire manipuler les opérations suivantes :

- une recherche simple dans une table
- une recherche plus compliquée avec éventuellement un comptage
- un tri pour, par exemple trouver les 5 premiers qui...
- une fusion

### Travail demandé

Préparer une série d'exercices dans ce but. Faire une liste des points difficiles où les élèves risquent d'avoir du mal. Quelles indications mettez-vous en place pour débloquer les élèves sans donner les solutions ? Proposer un devoir maison pour évaluer les apprentissages de cette séance.