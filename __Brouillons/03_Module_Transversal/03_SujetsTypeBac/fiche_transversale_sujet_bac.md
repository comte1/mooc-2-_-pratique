# Fiche transversale

## Sujet bac

Votre IA-IPR vous contacte début juin afin que vous participiez à l’élaboration d’un sujet de bac pour l’année suivante.

Rappels :
- Un sujet de bac comporte 5 exercices, l’élève devra en traiter 3 sur les 5. 
- Chaque exercice est noté sur 4 points. 
- Les 5 exercices devront porter sur des thèmes différents du programme. 
- Dans la mesure du possible, les 5 exercices devront être de difficulté équivalente. 
- Les deux premières questions de chaque exercice devront être “faciles”.

Proposez un sujet de bac ainsi que son barème et sa correction

