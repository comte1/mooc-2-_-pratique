# Programmation Python -- Fiche prof

Cette [_fiche professeur_](Ressources/fiche_prof_01.md) détaille le scénario associée à la fiche d'activité sur la programmation Python présentée au module précédent (voir  l'[activité 2](../../1_Penser-Concevoir-Elaborer/Niveau_1_Utiliser/Activite_02.md)). 

Faites une critique de ce scénario.