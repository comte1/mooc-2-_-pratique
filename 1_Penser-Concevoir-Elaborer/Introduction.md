Il s'agit dans ce premier module de s'entraîner à détecter _ce qui va et ce qui ne va pas_ dans une activité ou une suite d'activités :

- comprendre les objectifs (connaissances ou compétences)
- identifier les pré-requis
- juger de la pertinence

Vous pourrez commencer le module par l'analyse de fiches _exemples_ d'activités, fiches plus ou moins bien construites ; puis continuerez en proposant vos propres fiches, soit par duplication / adaptation de contenus existants soit à partir d'une idée originale. Les nouvelles fiches ainsi créées pourront à leur tour alimenter les discussions.
