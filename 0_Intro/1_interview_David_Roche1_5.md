# Enseigner l'informatique au secondaire

Cette série d'interviews a pour but de recueillir l'expérience d'enseignants et d'enseignantes de NSI (spécialité Numérique et Sciences Informatiques):
comment sont-ils arrivés à enseigner cette discipline, quelles difficultés ont-ils rencontrées, quelle pédagogie ont-ils mis en oeuvre, ...  ? Ils et elles témoignent pour partager leurs pratiques avec de jeunes venus dans l'enseignement de NSI.

## Interview de David Roche, enseignant en NSI au Lycée Guillaume Fichet de Bonneville

**Sommaire des 5 vidéos**

* **1/5 David Roche, qui es tu ?** 
* 2/5 [Comment enseigner et préparer un cours de NSI ?](https://gitlab.com/comte1/mooc-2-_-pratique/-/blob/master/0_Intro/1_interview_David_Roche2_5.md) 
* 3/5 [Pratiques en classe\.](https://gitlab.com/comte1/mooc-2-_-pratique/-/blob/master/0_Intro/1_interview_David_Roche3_5.md)
* 4/5 [Pratiques en conditions dégradées\.](https://gitlab.com/comte1/mooc-2-_-pratique/-/blob/master/0_Intro/1_interview_David_Roche4_5.md)
* 5/5 [Enjeux d'égalité entre filles et garçons\.](https://gitlab.com/comte1/mooc-2-_-pratique/-/blob/master/0_Intro/1_interview_David_Roche5_5.md)



## 1/5 David Roche, qui es tu ?

Dans cette vidéo David Roche nous raconte son parcours de professeur de Physique qui l'a conduit à l'enseignement de NSI 

[Interview David Roche 1/5](https://files.inria.fr/LearningLab_public/C045TV/ITW/NSI-ITW-DROCHE-1.mp4)
